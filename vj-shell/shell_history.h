/**
  ******************************************************************************
  * @file    shell-history.h
  * @author  Iron
  * @date    2021-02-02
  * @version v1.0
  * @brief   shell-history header file
  */

#ifndef __SHELL_HISTORY_H
#define __SHELL_HISTORY_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include "shell.h"

/* Exported define -----------------------------------------------------------*/

#define SHELL_HISTORY_NUM   10

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/

int shell_history_init(struct shell_history_queue_t *history);

void shell_history_reset(shell_context_t *shell);

int shell_history_append(shell_context_t *shell, const char *cmd, int len);

int shell_history_last(shell_context_t *shell, char *buf, int bufsize);

int shell_history_next(shell_context_t *shell, char *buf, int bufsize);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __TEMPLATE_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
