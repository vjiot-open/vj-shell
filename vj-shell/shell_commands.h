/**
  ******************************************************************************
  * @file    shell-commands.h
  * @author  Iron
  * @date    2021-01-29
  * @version v1.0
  * @brief   shell-commands header file
  */

#ifndef __SHELL_COMMANDS_H
#define __SHELL_COMMANDS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include "shell.h"

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/

int shell_commands_init(struct shell_command_sets_t *sets);

int shell_command_set_register(struct shell_command_sets_t *sets, struct shell_command_set_t *set);

int shell_command_set_deregister(struct shell_command_sets_t *sets, struct shell_command_set_t *set);

const struct shell_command_t *shell_command_lookup(struct shell_command_sets_t *sets, const char *name, int len, int search_index);

const char *shell_commands_search(struct shell_command_sets_t *sets, const char *cmd_name, int len, int search_index);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __SHELL_COMMANDS_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
