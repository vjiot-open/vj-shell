/**
  ******************************************************************************
  * @file    shell_commands.c
  * @author  Iron
  * @date    2021-01-29
  * @version v1.0
  * @brief   shell_commands c file
  */

/** @addtogroup GROUP_SHELL
* @{
*/

/* Private includes ----------------------------------------------------------*/
#include "shell_commands.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static struct shell_command_set_t builtin_shell_command_set;

/* Private function prototypes -----------------------------------------------*/

/*---------------------------------------------------------------------------*/
int shell_commands_init(struct shell_command_sets_t *sets)
{
    shell_assert(sets);

    /* Initialize the list. */
    SLIST_INIT(sets);

    /* Initialize the builtin shell commands list. */
    SLIST_INSERT_HEAD(sets, &builtin_shell_command_set, next);

    return 0;
}

/*---------------------------------------------------------------------------*/
int shell_command_set_register(struct shell_command_sets_t *sets, struct shell_command_set_t *set)
{
    struct shell_command_set_t *var = NULL;

    shell_assert(sets);

    if (SLIST_EMPTY(sets))
    {
        SLIST_INSERT_HEAD(sets, set, next);
    }
    else
    {
        SLIST_FOREACH(var, sets, next)
        {
            if (var->next.sle_next == NULL)
            {
                SLIST_INSERT_AFTER(var, set, next);
                break;
            }
        }
    }

    return 0;
}

/*---------------------------------------------------------------------------*/
int shell_command_set_deregister(struct shell_command_sets_t *sets, struct shell_command_set_t *set)
{
    shell_assert(sets);

    SLIST_REMOVE(sets, set, shell_command_set_t, next);

    return 0;
}

/*---------------------------------------------------------------------------*/
/* len = 0 表示全字匹配， len != 0 返回第一个匹配的命令 */
const struct shell_command_t *shell_command_lookup(struct shell_command_sets_t *sets, const char *cmd_name, int len, int search_index)
{
    struct shell_command_set_t *set;

    shell_assert(sets);

    SLIST_FOREACH(set, sets, next)
    {
        for (int i = 0; i < set->commands_num; i++)
        {
            const struct shell_command_t *cmd = &set->commands[i];

            if ((len > 0 && !strncmp(cmd->name, cmd_name, len)) || !strcmp(cmd->name, cmd_name))
            {
                if (search_index == 0)
                {
                    return cmd;
                }

                search_index--;
            }
        }
    }

    return NULL;
}

const char *shell_commands_search(struct shell_command_sets_t *sets, const char *cmd_name, int len, int search_index)
{
    const struct shell_command_t *cmd;

    cmd = shell_command_lookup(sets, cmd_name, len, search_index);

    return (cmd == NULL) ? NULL : cmd->name;
}

/*---------------------------------------------------------------------------*/
static char cmd_help(shell_context_t *shell, char *args)
{
    struct shell_command_sets_t *sets = shell->command_sets;
    struct shell_command_set_t *set;
    const struct shell_command_t *cmd;

    shell_assert(shell);
    shell_assert(shell->command_sets);

    shell->shell_printf("Available commands:"  SHELL_NEW_LINE);

    /* Note: we explicitly don't expend any code space to deal with shadowing */
    SLIST_FOREACH(set, sets, next)
    {
        for (int i = 0; i < set->commands_num; i++)
        {
            cmd = &set->commands[i];

            if (cmd && cmd->help)
            {
                shell->shell_printf("%s" SHELL_NEW_LINE, cmd->help);
            }
        }
    }

    return 0;
}

/*---------------------------------------------------------------------------*/
static char cmd_clear(shell_context_t *shell, char *args)
{
    shell_assert(shell);

    shell->shell_printf("\033[2J"); // VT100 终端控制码，清屏
    shell->shell_printf("\033[%d;%dH", 0, 0); // VT100 终端控制码，光标移到 y,x

    return 0;
}

/*---------------------------------------------------------------------------*/
static char cmd_history(shell_context_t *shell, char *args)
{
    struct shell_history_head_t *phead;
    struct shell_history_t *elm;
    int index;

    shell_assert(shell);

    if (shell->history == NULL)
    {
        shell->shell_printf("Shell history not init." SHELL_NEW_LINE);
        return 0;
    }

    shell->shell_printf("shell history queue:"  SHELL_NEW_LINE);

    phead = &shell->history->head;
    index = 0;

    TAILQ_FOREACH(elm, phead, entries)
    {
        index++;

        shell->shell_printf("%d: ", index);
        shell->shell_puts(elm->commands);
        shell->shell_puts(SHELL_NEW_LINE);
    }

    return 0;
}

/*---------------------------------------------------------------------------*/
const struct shell_command_t builtin_shell_commands[] =
{
    { "?",                    cmd_help,                 " > ?:               Shows this help" },
    { "help",                 cmd_help,                 " > help:            Shows this help" },
    { "clear",                cmd_clear,                " > clear:           Clear the screen" },
    { "history",              cmd_history,              " > history:         List shell history input." },
};

static struct shell_command_set_t builtin_shell_command_set =
{
    .next = NULL,
    .commands = builtin_shell_commands,
    .commands_num = sizeof(builtin_shell_commands) / sizeof(struct shell_command_t),
};

/**
  * @}
  */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
