/**
  ******************************************************************************
  * @file    types.h 
  * @author  Iron
  * @date    2022-01-01
  * @version v1.0
  * @brief   types header file
  */

#ifndef __TYPES_H
#define __TYPES_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Exported includes ---------------------------------------------------------*/
#include <stddef.h>
#include <stdint.h>

/* Exported define -----------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#ifndef ssize_t
typedef int32_t ssize_t;
#endif

/* Exported types ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __TYPES_H */

/******************* (C)COPYRIGHT 2022 ***** END OF FILE *********************/
