/**
  ******************************************************************************
  * @file    main.c
  * @author  Iron
  * @date    2021-05-20
  * @version v1.0
  * @brief   main c file
  */

/* Private includes ----------------------------------------------------------*/
#include <stdlib.h>
#include "shell.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define USER_NAME 					"xwiron"
#define USER_EMAILE					"xwiron@aliyun.com"

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* serial helle omessage */
static const char* wellcom =
SHELL_NEW_LINE
"Mini Terminal Shell For MCUs" SHELL_NEW_LINE
"Copyright (c) 2021, Author: " USER_NAME ", Email: " USER_EMAILE SHELL_NEW_LINE
"Type \"help\" to view a list of registered commands" SHELL_NEW_LINE
SHELL_NEW_LINE;

/* shell context */
static shell_context_t       shell_context;
struct shell_command_sets_t  shell_command_sets;
struct shell_history_queue_t shell_command_history;

/* Shell buffer */
#define	SHELL_BUF_SIZE 128
static char shell_rxbuf[SHELL_BUF_SIZE];

/* Private function prototypes -----------------------------------------------*/

/* shell 自定义命令 ----------------------------------------------------------*/
static char cmd_test(shell_context_t *shell, char *args)
{
	shell->shell_printf("test command runing..." SHELL_NEW_LINE);

	return 0;
}

const struct shell_command_t app_shell_commands[] =
{
    { "test", cmd_test, " > test:            test command" },
};

static struct shell_command_set_t app_shell_command_sets =
{
    .next = NULL,
    .commands = app_shell_commands,
    .commands_num = sizeof(app_shell_commands) / sizeof(struct shell_command_t),
};

/* shell 自定义命令2，利用宏定义基于链接器 section 存储的测试命令 --------*/
#if (defined(__ICCARM__))
static char cmd_test(shell_context_t *shell, char *args)
{
	shell->shell_printf("test2 command runing..." SHELL_NEW_LINE);

	return 0;
}

SHELL_CMD_REGISTER(test2, cmd_test2, "test2 command");

static struct shell_command_set_t *app_shell_commad_section_set(void)
{
    extern const uint32_t SHELL_CMD_RO_SECTION$$Base;
    extern const uint32_t SHELL_CMD_RO_SECTION$$Limit;
    static struct shell_command_set_t shell_command_ro_section_set;

    shell_command_ro_section_set.next = NULL;
    shell_command_ro_section_set.commands = (const struct shell_command_t*)(&SHELL_CMD_RO_SECTION$$Base);
    shell_command_ro_section_set.commands_num = ((uint32_t)(&SHELL_CMD_RO_SECTION$$Limit) - (uint32_t)(&SHELL_CMD_RO_SECTION$$Base)) / sizeof(struct shell_command_t);
    
    return &shell_command_ro_section_set;
}
#endif

/*----------------------------------------------------------------------------*/
void shell_run(shell_context_t *shell, const char *name)
{
    shell->shell_name    = name;
    shell->shell_getchar = getchar;
    shell->shell_putchar = putchar;
    shell->shell_puts    = puts;
    shell->shell_printf  = printf;
    shell->shell_malloc  = malloc;
    shell->shell_free    = free;

    //shell_low_level_init();

    shell_init(shell, &shell_command_sets, &shell_command_history);

    shell_command_set_register(&shell_command_sets, &app_shell_command_sets); // 命令表模式

#if (defined(__ICCARM__))
    //shell_command_set_register(&shell_command_sets, app_shell_commad_section_set()); // Section 模式命令定义
#endif

    shell->shell_puts(
        SHELL_NEW_LINE
        "Shell init done, Press any key to continue..."
        SHELL_NEW_LINE
    );

    shell->shell_getchar();

    shell->shell_puts(wellcom);

    shell_output_prompt(shell);

    for (;;)
    {
        memset(shell_rxbuf, 0, SHELL_BUF_SIZE);

        shell_getline(shell, shell_rxbuf, SHELL_BUF_SIZE);

        shell_input(shell, shell_rxbuf);
    }
}

int main(void)
{
    shell_run(&shell_context, "vj-Shell");
}


/**
* @}
*/

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
