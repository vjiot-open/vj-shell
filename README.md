# vj-shell
  适用于 MCU 的串口交互终端，PC 端软件推荐 xShell, putty, MobaXterm 等支持 xterm, vt100 终端类型的工具软件。注意不同终端回车换行显示配置区别。

# Win32 build test
```
cmake -Awin32 -Bbuild .
cmake --build .\build
.\build\Debug\vj-shell.exe
```

# Cygwin build
```
cmake -Bbuild .
cmake --build ./build
./build/vj-shell.exe
```

# Win32 shell test
![vj-shell](images/vj-shell.jpg)

# MCU threadx shell test
![vj-shell](images/vj-shell_serial_test.gif)

